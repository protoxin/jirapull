# JiraPull

JiraPull is just a simple python script to view the remote jira version of a target. 

Yes, you can do this yourself via a web browser. The idea here is to provide a tool to do it. 

Installation: `pip3 install -r requirements.txt `

Usage: `./jira_pull.py https://target.ip ` adding `http://` or `https://` is required for now. 

```
###################################################
#  JiraPull - Enumerate JIRA Version Information  #
###################################################

[+]Target:  https://jira.xxxxxxxx.com
[+]Error parsing html from target.
[+]Trying /secure/Dashboard.jspa path.
[+]Target appears to be running JIRA!
[+]Remote JIRA version:  8.x.x
[+]Current JIRA version is: 8.0.x

```