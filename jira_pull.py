#!/usr/bin/env python3

import sys
import requests
from bs4 import BeautifulSoup
# Suppress warning that ssl cert is invalid. 
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def jirapull1(target):
	''' Function to parse html and attempt to pull jira version '''
	try:
		jiraTarget = requests.get(target,verify=False)
	except:
		print("[+]ERROR: Could not connect to: ", target)
		exit()
	content = jiraTarget.content
	soup = BeautifulSoup(content,features="html.parser")
	try:
		# jira has a <meta> tag called application-name that can be parsed to
		# view the remote version
		desc = soup.find(attrs={'name':'application-name'})
		if "jira" in desc['data-name']:
			print("[+]Target appears to be running JIRA!")
		else:
			pass
	except:
		print("[+]Error parsing html from target.")
		print("[+]Trying /secure/Dashboard.jspa path.")
		try:
			# Reset and reparse. the /secure/Dashboard path is pretty usable in our testing.
			target = target+"/secure/Dashboard.jspa"
			jiraTarget = requests.get(target,verify=False)
			content = jiraTarget.content
			soup = BeautifulSoup(content,features="html.parser")
			desc = soup.find(attrs={'name':'application-name'})
			if "jira" in desc['data-name']:
				print("[+]Target appears to be running JIRA!")
			else:
				pass
		except:
			print("[+]Could not determine if instance is parsable. Exiting...")
			exit()
	jVersion = desc['data-version']
	print("[+]Remote JIRA version: ", jVersion)
	if jVersion < "7.4":
		print("[+]Remote JIRA version is EOL!")
		print("[+]For EOL dates, refer to: https://confluence.atlassian.com/support/atlassian-support-end-of-life-policy-201851003.html")
	elif jVersion > "7.4":
		print("[+]Remote JIRA version is still supported.")
	else:
		pass

if len(sys.argv) <= 1:
	print("Usage: ./jira_pull.py https://target.ip ---- http/https is required.")
	exit()
else:
	pass
target = sys.argv[1]
print("\n")
print("###################################################")
print("#  JiraPull - Enumerate JIRA Version Information  #")
print("###################################################\n")
print("[+]Target: ",target)
jirapull1(target)
print("\n")
